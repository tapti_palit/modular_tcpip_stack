#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/types.h>

static int __init tcp_mod_init(void)
{
  return 0;
}

static void __exit tcp_mod_fini(void)
{
  return 0;
}

module_init(tcp_mod_init);
module_exit(tcp_mod_fini);

MODULE_LICENSE("GPL");
MODULE_DESCRIPTION("Module for TCP IP stack");
//MODULE_AUTHOR("");
